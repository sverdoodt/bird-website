// Import the birds from the data.mjs file.
import { data } from './data.mjs';

var numberCards = 0;

// Variables for the cards and indexing. We want 10 cards if we're on mobile
if (window.innerWidth < 480) {
    numberCards = 10;
    document.querySelector(".bonusCard").style.display = "inline";
} else {
    numberCards = 9;
    document.querySelector(".bonusCard").style.display = "none";
    document.querySelector("#backToGrid").style.display = "none";
}


// Get all the required containers to loop over.
var birdCategories = Array.from(document.querySelectorAll(".birdCategory"));
var cards = Array.from(document.querySelectorAll(".card"));
cards.push(document.querySelector(".bonusCard"));
var images = Array.from(document.querySelectorAll(".image"));
var birdNames = Array.from(document.querySelectorAll(".birdName"));

// Loop over the data to get all the birdnames (of a category, when specified) and puts 
// them in alphabetical order.
function getAlphabeticalBirds(category) {
    var birdsAndIndices = [];
    
    for (let i=0; i<data.length; i++) {
        if (category == null) {
            birdsAndIndices.push([i, data[i].naam]);
        } else if (category === data[i].categorie) {
            birdsAndIndices.push([i, data[i].naam]);
        } else if (category === "Zangvogels" && 
        ["Kleine Zangvogels", "Kleine Zangvogels niet Europa", "Grote Zangvogels"].includes(data[i].categorie)) {
            birdsAndIndices.push([i, data[i].naam]);
        }
    }

    return birdsAndIndices;
}

// We want to sort the birds in alphabetical order and this keeps the indices correct
function cmp(a, b) {
    return a[1].localeCompare(b[1]);
}

var birdsAndIndices = getAlphabeticalBirds();
var birdsSorted = birdsAndIndices.sort(cmp);

// For the indices, we have to keep track on which page we are
const firstNavigator = document.querySelector("#firstNavigator");
const previousNavigator = document.querySelector("#previousNavigator");
const nextNavigator = document.querySelector("#nextNavigator");
const lastNavigator = document.querySelector("#lastNavigator");
const navigators = [firstNavigator, previousNavigator, nextNavigator, lastNavigator];

var indexState = 0;

// Function that listens a click on one of the categories (in a function, because this needs to be repeated)
function listenCategories(birdCategories) {
    birdCategories.forEach(birdCategory => {
        birdCategory.addEventListener("click", function show() {
            indexState = 0;
            birdsAndIndices = getAlphabeticalBirds(birdCategory.innerHTML);
            birdsSorted = birdsAndIndices.sort(cmp);

            // Get the right view
            document.querySelector("#vogelCards").style.display = "grid";
            document.querySelector("#paging").style.display = "flex";
            document.querySelector("#vogelDetailed").style.display = "none";
    
            // Same problem as before: for some categories, we don't have 9/10 birds. We set the display to 
            // none there, and this is reset here
            cards.forEach((card) => {
                card.style.display="inline";
            });
    
            // Set the selected category a slightly darker grey, all the rest the same whitesmoke
            birdCategories.forEach((category) => {
                category.style.background = 'WhiteSmoke';
            })
    
            birdCategory.style.background = "LightGrey";
        
            for (let i=0; i<numberCards; i++) {
                try {
                    images[i].src = data[birdsSorted[i][0]].path;
                    birdNames[i].innerHTML = data[birdsSorted[i][0]].naam;
                } catch(err) {
                    cards[i].style.display = "none";
                }
            }
        })
    })
}

// Loops over all the cards and sets the image and name (definitely not the best way to do this, because
// I basically repeat everything. This should be put in a function)
function loadCatalogue() {
    
    // In the case of the indexing, at some point we get to the end and there aren't 9/10 birds left. We set
    // the display to none there. This needs to be reset though.
    cards.forEach((card) => {
        card.style.display = "inline";
    });

    for (let i=0; i<numberCards; i++) {
        try {
            images[i].src = data[birdsSorted[i+indexState*numberCards][0]].path;
            birdNames[i].innerHTML = data[birdsSorted[i+indexState*numberCards][0]].naam;
        } catch(err) {
            cards[i].style.display = "none";
        }

        listenCategories(birdCategories);
    }
}

navigators.forEach(navigator => {
    navigator.addEventListener("click", function navigate() {
        if (navigator.innerHTML === "First") {
            indexState = 0;
            loadCatalogue();
        } else if (navigator.innerHTML === "Previous" && indexState > 0) {
            indexState--;
            loadCatalogue();
        } else if (navigator.innerHTML === "Next" && indexState * numberCards <= birdsAndIndices.length - numberCards) {
            indexState++;
            loadCatalogue();
        } else if (navigator.innerHTML === "Last") {
            while (indexState * numberCards <= birdsAndIndices.length - numberCards) {
                indexState++;
            }
            loadCatalogue();
        }
    })
})

// Some code to fill the searchbar
const alphabeticalBirds = getAlphabeticalBirds().sort(cmp);
var birdOption = document.getElementById("birdOption");
birdOption.value = alphabeticalBirds[0][1];

for (let i=1; i<data.length; i++) {
    var cloneOption = birdOption.cloneNode(true);
    cloneOption.value = alphabeticalBirds[i][1];
    document.getElementById("possibleBirds").appendChild(cloneOption);
}

// Now that the searchbar is filled, we can listen to which bird is clicked
const input = document.querySelector('.input');
let eventSource = null;

// This function transitions from general view to detailed view
function displayDetailedView (bird) {
    document.querySelector("#vogelCards").style.display = "none";
    document.querySelector("#paging").style.display = "none";
    document.querySelector("#vogelDetailed").style.display = "flex";
    if (window.innerWidth < 480) {
        document.querySelector("#backToGrid").style.display = "flex";
    }
    

    // reset the none display you do later
    document.querySelector(".detailedBirdFact").style.display = "inline";
    document.querySelector(".volumeLogo").style.display = "inline";

    // Cycle through all the birds to see if it matches the value clicked
    // if the bird has audio and a fact, display it, otherwise don't
    // There is quite a lot of styling that is dependent on whether or not it is the general 
    // or the detailed view
    for (let i=0; i<data.length; i++) {
        if (data[i].naam == bird) {
            document.querySelector(".detailedImage").src=data[i].path;
            document.querySelector(".detailedBirdName").innerHTML=data[i].naam;

            if (data[i].audiopath != null) {
                document.querySelector(".detailedBirdFact").innerHTML=data[i].text;
                document.querySelector(".vogelDetailedCard").style.padding = "0px 0px 15px 0px";
                document.querySelector(".detailedBird").style.padding = "0px 0px 10px 0px";
                
                let audio = document.getElementById("audioBird");
                let button = document.querySelector(".fa-volume-high");
                audio.src=data[i].audiopath;
                button.addEventListener("click", function play(e) {
                    e.stopImmediatePropagation();
                    if (audio.paused) {
                        audio.play();
                    } else {
                        audio.pause();
                        audio.currentTime = 0;
                    }
                })
            } else {
                document.querySelector(".detailedBirdFact").style.display = "none";
                document.querySelector(".volumeLogo").style.display = "none";
                document.querySelector(".vogelDetailedCard").style.padding = "0px 0px 0px 0px";
                document.querySelector(".detailedBird").style.padding = "0px 0px 0px 0px";
            }
        }
    }
}

// This listens to the input of the searchbar
input.addEventListener('keypress', function (e) {
    if (e.key === 'Enter') {
      let bird = e.target.value;
      let initcapBird = bird[0].toUpperCase() + bird.substring(1);
      displayDetailedView(initcapBird);
    }
})
input.addEventListener('keydown', (e) => {
    eventSource = e.key ? 'input' : 'list';
})
input.addEventListener('input', (e) => {
    if (eventSource === 'list') {
        let bird = e.target.value;
        displayDetailedView(bird);
    }
})

// In the case of a mobile detailed view, we have a 'return to catalogue',
// this also needs logic
let overview = document.querySelector("#backToGrid");
overview.addEventListener('click', function show() {
    document.querySelector("#vogelCards").style.display = "grid";
    document.querySelector("#paging").style.display = "flex";
    document.querySelector("#vogelDetailed").style.display = "none";
    document.querySelector("#backToGrid").style.display = "none";
})

// This loads the pictures the first time
loadCatalogue();