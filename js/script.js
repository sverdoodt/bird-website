// Import the birds from the data.mjs file.
import { data } from './data.mjs';

// We can only use birds of which the text is not null.
const dataFacts = data.filter(bird => (bird.text));

// Variables for the cards and indexing.
const numberCards = 9;

// Get all the required containers to loop over.
const cards = Array.from(document.querySelectorAll(".card"));
const images = Array.from(document.querySelectorAll(".image"));
const birdNames = Array.from(document.querySelectorAll(".birdName"));
const birdFacts = Array.from(document.querySelectorAll(".birdFact"));

// Helper functions to shuffle an array (used for shuffling the indices).
function shuffleArray(array) {
    let currentIndex = array.length,  randomIndex;
    while (currentIndex != 0) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }
    return array;
}

// Helper function to get a random set of n indices.
function getIndices(n) {
    let indices = [];

    for (let i=0; i<dataFacts.length;i++) {
        indices.push(i);
    }

    return shuffleArray(indices).slice(0, n);
}

// I want to save these indices for one day and then have new ones the day after. It now stores
// an object with the date and the indices. When the page is loaded in, it checks if the date is
// the same. If it is, it uses those indices, if not, it generates new ones. This solution works,
// but is different on each machine (which is fine for me).
if (!localStorage.getItem("indices")) {
    var indices = getIndices(numberCards);
    var dateIndices = {
        "date": Date().toString().slice(4, 15),
        "indices": indices
    };
    localStorage.setItem("indices", JSON.stringify(dateIndices));
} else {
    var dateIndices = JSON.parse(localStorage.getItem("indices"));
    if (dateIndices.date === Date().toString().slice(4, 15)) {
        indices = dateIndices.indices;
    } else {
        indices = getIndices(numberCards);
        var dateIndices = {
            "date": Date().toString().slice(4, 15),
            "indices": indices
        };
        localStorage.setItem("indices", JSON.stringify(dateIndices));
    }
}

// Loops over all the cards and sets the containers, using the indices defined above. We
// set the image, name, text and audio. We add a function to the audio so multiple clicks
// pause and set the time to zero, so it restarts on the next click.
for (let i=0; i<numberCards; i++) {
    images[i].src = dataFacts[indices[i]].path.substring(3);
    birdNames[i].innerHTML = dataFacts[indices[i]].naam;
    birdFacts[i].innerHTML = dataFacts[indices[i]].text;

    let audio = document.getElementById("audioBird"+i);
    audio.src = dataFacts[indices[i]].audiopath.substring(3);
    cards[i].addEventListener("click", function play() {
        if (audio.paused) {
            audio.play();
        } else {
            audio.pause();
            audio.currentTime = 0;
        }
    })
}
