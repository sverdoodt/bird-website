// Voor copy-paste purposes
const categories = [
    "Loopvogels",
    "Eendvogels",
    "Hoendervogels",
    "Pinguins",
    "Buissnaveligen",
    "Waadvogels",
    "Roofvogels",
    "Scharrelaarsvogels",
    "Papegaaien",
    "Kleine zangvogels",
    "Kleine zangvogels niet Europa",
    "Grote zangvogels"
]

export const data = [
    {
        "naam": "Struisvogel",
        "categorie": "Loopvogels",
        "path": "../images/Struisvogel.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": "groot",
        "audiopath": "../audio/Struisvogel.mp3",
        "text": "De struisvogel is de grootste en zwaarste nog levende vogel en met snelheden tot 70km/h ook de snelste loopvogel ter wereld. Dit is mede mogelijk gemaakt door de speciale bouw van hun voet. Struisvogels hebben twee tenen waar de meeste vogels er vier hebben."
    },
    {
        "naam": "Kasuaris",
        "categorie": "Loopvogels",
        "path": "../images/Kasuaris.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Emoe",
        "categorie": "Loopvogels",
        "path": "../images/Emoe.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Kiwi",
        "categorie": "Loopvogels",
        "path": "../images/Kiwi.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": "klein",
        "audiopath": "../audio/Kiwi.mp3",
        "text": "Met een grootte tussen de 25cm en 45cm is de kiwi de kleinste niet-vliegende vogel. Hij ondervindt vooral de nadelen van niet-vliegende vogelsoorten zonder de voordelen van grootte, die hier meestal mee gepaard gaan. Daardoor moet hij vooral 's nachts op zoek naar eten. De kiwi is de vogel met het grootste ei in verhouding tot het lichaam."
    },
    {
        "naam": "Nandoe",
        "categorie": "Loopvogels",
        "path": "../images/Nandoe.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Tinamoe",
        "categorie": "Loopvogels",
        "path": "../images/Tinamoe.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Kraanvogel",
        "categorie": "Loopvogels",
        "path": "../images/Kraanvogel.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Grote Trap",
        "categorie": "Loopvogels",
        "path": "../images/Grote Trap.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Mandarijneend",
        "categorie": "Eendvogels",
        "path": "../images/Mandarijneend.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": "klein",
        "audiopath": "../audio/Mandarijneend.mp3",
        "text": "De mandarijneend is een kleine eendsoort die vooral in Zuid-Oost Azië voorkomt. Atypisch voor deze vogel is dat de vrouwtjes het initiatief nemen bij het kiezen van een partner. Eenmaal een partner gevonden, blijven deze eenden voor het leven bij elkaar."
    },
    {
        "naam": "Wilde Eend",
        "categorie": "Eendvogels",
        "path": "../images/Wilde Eend.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Grauwe Gans",
        "categorie": "Eendvogels",
        "path": "../images/Grauwe Gans.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Canadese Gans",
        "categorie": "Eendvogels",
        "path": "../images/Canadese Gans.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": "groot",
        "audiopath": "../audio/Canadese Gans.mp3",
        "text": "De Canadese gans is de grootste wilde gans die voornamelijk voortkomt in Noord-Amerika, maar soms ook naar Europa overvliegt. In de gekende V-formatie kunnen deze vogels tot meer dan 2000km afleggen op 24 uur. Deze gans is een heel agressieve vogel naar zowel andere dieren als mensen."
    },
    {
        "naam": "Knobbelzwaan",
        "categorie": "Eendvogels",
        "path": "../images/Knobbelzwaan.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Fuut",
        "categorie": "Eendvogels",
        "path": "../images/Fuut.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Waterhoen",
        "categorie": "Eendvogels",
        "path": "../images/Waterhoen.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Carolina Eend",
        "categorie": "Eendvogels",
        "path": "../images/Carolina Eend.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "IJseend",
        "categorie": "Eendvogels",
        "path": "../images/IJseend.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Bergeend",
        "categorie": "Eendvogels",
        "path": "../images/Bergeend.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Nijlgans",
        "categorie": "Eendvogels",
        "path": "../images/Nijlgans.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Zwaangans",
        "categorie": "Eendvogels",
        "path": "../images/Zwaangans.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Slobeend",
        "categorie": "Eendvogels",
        "path": "../images/Slobeend.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": "klein",
        "audiopath": "../audio/Slobeend.mp3",
        "text": "De slobeend is te herkennen aan zijn lepelachtige snavel die hij gebruikt om kleine schaaldieren en zaden uit het water te vissen. De slobeend is hierin vrij uniek, gezien het merendeel van de eenden herbivoor is. In vergelijking met andere eendsoorten, is de slobeend ook sterk territoriaal."
    },
    {
        "naam": "Pacifische Parelduiker",
        "categorie": "Eendvogels",
        "path": "../images/Pacifische Parelduiker.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Zwanenhalsfuut",
        "categorie": "Eendvogels",
        "path": "../images/Zwanenhalsfuut.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Meerkoet",
        "categorie": "Eendvogels",
        "path": "../images/Meerkoet.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Parelhoen",
        "categorie": "Hoendervogels",
        "path": "../images/Parelhoen.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Kwartel",
        "categorie": "Hoendervogels",
        "path": "../images/Kwartel.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": "klein",
        "audiopath": "../audio/Kwartel.mp3",
        "text": "De kwartel is, hoewel een vrij stuntelige vlieger, de enige trekvogel uit de orde der hoendervogels. Hij is heel schuw, maar zal eerder wegrennen of dood spelen dan wegvliegen. Ze laten zich echter wel horen met hun herkenbare roep: “kwik-me-dit”."
    },
    {
        "naam": "Fazant",
        "categorie": "Hoendervogels",
        "path": "../images/Fazant.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Korhoen",
        "categorie": "Hoendervogels",
        "path": "../images/Korhoen.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Pauw",
        "categorie": "Hoendervogels",
        "path": "../images/Pauw.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Hokko",
        "categorie": "Hoendervogels",
        "path": "../images/Hokko.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Boskalkoen",
        "categorie": "Hoendervogels",
        "path": "../images/Boskalkoen.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Patrijs",
        "categorie": "Hoendervogels",
        "path": "../images/Patrijs.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": "klein",
        "audiopath": "../audio/Patrijs.mp3",
        "text": "Patrijzen zijn heel gebrekkige vliegers, waardoor ze het hele jaar in dezelfde omgeving vertoeven. Gezien ze op de grond nesten zijn ze echter vaak een makkelijke prooi voor roofdieren, zoals vossen. De patrijs legt wel 15 tot 20 eieren per nest, hetgeen bij de meeste van alle vogelsoorten is."
    },
    {
        "naam": "Woestijnpatrijs",
        "categorie": "Hoendervogels",
        "path": "../images/Woestijnpatrijs.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Kwartelkoning",
        "categorie": "Hoendervogels",
        "path": "../images/Kwartelkoning.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Gekuifde Bospatrijs",
        "categorie": "Hoendervogels",
        "path": "../images/Gekuifde Bospatrijs.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Keizerspinguin",
        "categorie": "Pinguins",
        "path": "../images/Keizerspinguin.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": "groot",
        "audiopath": "../audio/Keizerspinguin.mp3",
        "text": "De keizerspingu&#239n is de grootste en zwaarste levende puingu&#239n en komt uitsluitend voort in Antarctica. Ze zijn de enige pengu&#239nsoort die tijdens de Antarctische winter broedt. Het vrouwtje legt &#233&#233n ei en het mannetje broedt deze de volgende twee maanden uit. "
    },
    {
        "naam": "Rotspinguin",
        "categorie": "Pinguins",
        "path": "../images/Rotspinguin.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Dwergpinguin",
        "categorie": "Pinguins",
        "path": "../images/Dwergpinguin.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Ezelspinguin",
        "categorie": "Pinguins",
        "path": "../images/Ezelspinguin.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Zwartvoetpinguin",
        "categorie": "Pinguins",
        "path": "../images/Zwartvoetpinguin.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Albatros",
        "categorie": "Buissnaveligen",
        "path": "../images/Albatros.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": "groot",
        "audiopath": "../audio/Albatros.mp3",
        "text": "Van alle vogels is de albatros de soort die op een jaar gemiddeld het langst in de lucht vertoeft. Dit is mogelijk omdat hij zo goed door de lucht glijdt. Een albatros kan een hele dag zonder zelfs één slag met zijn vleugels te moeten geven. Hij kan zelfs slapend door de lucht glijden."
    },
    {
        "naam": "Fregatvogel",
        "categorie": "Buissnaveligen",
        "path": "../images/Fregatvogel.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Aalscholver",
        "categorie": "Buissnaveligen",
        "path": "../images/Aalscholver.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Jan-van-gent",
        "categorie": "Buissnaveligen",
        "path": "../images/Jan-van-gent.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": "groot",
        "audiopath": "../audio/Jan-van-gent.mp3",
        "text": "De jan-van-gent is een zeevogel die in het noorden van de Atlantische Oceaan voorkomt. Daar jaagt hij met spectaculaire duiken tot 110km/h naar vis. De 'gent' komt van het middelengelse 'ganet', maar waar 'jan van' vandaan komt, blijft onzeker."
    },
    {
        "naam": "Kokmeeuw",
        "categorie": "Buissnaveligen",
        "path": "../images/Kokmeeuw.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Zilvermeeuw",
        "categorie": "Buissnaveligen",
        "path": "../images/Zilvermeeuw.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Mallemok",
        "categorie": "Buissnaveligen",
        "path": "../images/Mallemok.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Reuzenstormvogel",
        "categorie": "Buissnaveligen",
        "path": "../images/Reuzenstormvogel.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Roodsnavelkeerkringvogel",
        "categorie": "Buissnaveligen",
        "path": "../images/Roodsnavelkeerkringvogel.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Alk",
        "categorie": "Buissnaveligen",
        "path": "../images/Alk.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Stern",
        "categorie": "Buissnaveligen",
        "path": "../images/Stern.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Flamingo",
        "categorie": "Waadvogels",
        "path": "../images/Flamingo.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Ooievaar",
        "categorie": "Waadvogels",
        "path": "../images/Ooievaar.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Maraboe",
        "categorie": "Waadvogels",
        "path": "../images/Maraboe.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Reiger",
        "categorie": "Waadvogels",
        "path": "../images/Reiger.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Pelikaan",
        "categorie": "Waadvogels",
        "path": "../images/Pelikaan.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Ibis",
        "categorie": "Waadvogels",
        "path": "../images/Ibis.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Lepelaar",
        "categorie": "Waadvogels",
        "path": "../images/Lepelaar.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Grote Zilverreiger",
        "categorie": "Waadvogels",
        "path": "../images/Grote Zilverreiger.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Kwak",
        "categorie": "Waadvogels",
        "path": "../images/Kwak.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Hamerkop",
        "categorie": "Waadvogels",
        "path": "../images/Hamerkop.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Plevier",
        "categorie": "Waadvogels",
        "path": "../images/Plevier.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Kieviet",
        "categorie": "Waadvogels",
        "path": "../images/Kieviet.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Renvogel",
        "categorie": "Waadvogels",
        "path": "../images/Renvogel.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Kluut",
        "categorie": "Waadvogels",
        "path": "../images/Kluut.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Oeverloper",
        "categorie": "Waadvogels",
        "path": "../images/Oeverloper.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Watersnip",
        "categorie": "Waadvogels",
        "path": "../images/Watersnip.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Wulp",
        "categorie": "Waadvogels",
        "path": "../images/Wulp.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Tureluur",
        "categorie": "Waadvogels",
        "path": "../images/Tureluur.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Strandloper",
        "categorie": "Waadvogels",
        "path": "../images/Strandloper.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Havik",
        "categorie": "Roofvogels",
        "path": "../images/Havik.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Arend",
        "categorie": "Roofvogels",
        "path": "../images/Arend.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Buizerd",
        "categorie": "Roofvogels",
        "path": "../images/Buizerd.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Harpij",
        "categorie": "Roofvogels",
        "path": "../images/Harpij.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Gier",
        "categorie": "Roofvogels",
        "path": "../images/Gier.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Secretarisvogel",
        "categorie": "Roofvogels",
        "path": "../images/Secretarisvogel.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Sperwer",
        "categorie": "Roofvogels",
        "path": "../images/Sperwer.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Steenarend",
        "categorie": "Roofvogels",
        "path": "../images/Steenarend.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Havikarend",
        "categorie": "Roofvogels",
        "path": "../images/Havikarend.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Wouw",
        "categorie": "Roofvogels",
        "path": "../images/Wouw.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Roodstaartbuizerd",
        "categorie": "Roofvogels",
        "path": "../images/Roodstaartbuizerd.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Slangenarend",
        "categorie": "Roofvogels",
        "path": "../images/Slangenarend.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Kiekendief",
        "categorie": "Roofvogels",
        "path": "../images/Kiekendief.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Lammergier",
        "categorie": "Roofvogels",
        "path": "../images/Lammergier.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Californische Condor",
        "categorie": "Roofvogels",
        "path": "../images/Californische Condor.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Andescondor",
        "categorie": "Roofvogels",
        "path": "../images/Andescondor.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "IJsvogel",
        "categorie": "Scharrelaarvogels",
        "path": "../images/IJsvogel.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": "klein",
        "audiopath": "../audio/IJsvogel.mp3",
        "text": "De ijsvogel leeft uitsluitend van vis, waardoor hij ook sterk aan water gebonden is. IJsvogels hebben vaak een vaste tak die dient als uitkijkpost. Wanneer hij een prooi spot, vist hij deze uit het water aan snelheden van wel 80km/h."
    },
    {
        "naam": "Kookaburra",
        "categorie": "Scharrelaarvogels",
        "path": "../images/Kookaburra.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Todie",
        "categorie": "Scharrelaarvogels",
        "path": "../images/Todie.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Motmot",
        "categorie": "Scharrelaarvogels",
        "path": "../images/Motmot.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Bijeneter",
        "categorie": "Scharrelaarvogels",
        "path": "../images/Bijeneter.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Scharrelaar",
        "categorie": "Scharrelaarvogels",
        "path": "../images/Scharrelaar.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Dollarvogel",
        "categorie": "Scharrelaarvogels",
        "path": "../images/Dollarvogel.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Neushoornvogel",
        "categorie": "Papegaaien",
        "path": "../images/Neushoornvogel.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Toekan",
        "categorie": "Papegaaien",
        "path": "../images/Toekan.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Geelkuifkaketoe",
        "categorie": "Papegaaien",
        "path": "../images/Geelkuifkaketoe.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Roze Kaketoe",
        "categorie": "Papegaaien",
        "path": "../images/Roze Kaketoe.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Halsbandparkiet",
        "categorie": "Papegaaien",
        "path": "../images/Halsbandparkiet.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Ara",
        "categorie": "Papegaaien",
        "path": "../images/Ara.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": "groot",
        "audiopath": "../audio/Ara.mp3",
        "text": "De Ara is een van de grootste papegaaisoort ter wereld en komt vooral voor in Centraal- en Zuid-Amerika. Hij heeft een heel sterke snavel die hij vooral gebruikt om noten te kraken, waaronder zelfs kokosnoten. De Ara heeft ook een atypische tong, gezien er een bot in zit."
    },
    {
        "naam": "Kakapo",
        "categorie": "Papegaaien",
        "path": "../images/Kakapo.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": "groot",
        "audiopath": "../audio/Kakapo.mp3",
        "text": "De kakapo is de enige niet-vliegende papegaaisoort ter wereld. Typerend zijn onder andere zijn grote poten die gespecialiseerd zijn om in bomen te klimmen. Deze zijn echter wat onhandig tijdens het stappen, waardoor de kakapo vaak struikelt. Verkozen als vogel van het jaar in Nieuw-Zeeland in 2020. "
    },
    {
        "naam": "Amazone Papegaai",
        "categorie": "Papegaaien",
        "path": "../images/Amazone Papegaai.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Muspapegaai",
        "categorie": "Papegaaien",
        "path": "../images/Muspapegaai.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Goudparkiet",
        "categorie": "Papegaaien",
        "path": "../images/Goudparkiet.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Groene Specht",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Groene Specht.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Vink",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Vink.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Kanarie",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Kanarie.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Zwaluw",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Zwaluw.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Pimpelmees",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Pimpelmees.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": "klein",
        "audiopath": "../audio/Pimpelmees.mp3",
        "text": "De pimpelmees is een klein vogeltje dat je doorheen heel Europa kunt vinden, gezien het zich uitstekend heeft aangepast aan de menselijke omgeving. Hij vertoont veel gelijkenissen met zijn broertje, de koolmees, maar is te onderscheiden aan zijn blauwe toupet."
    },
    {
        "naam": "Koolmees",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Koolmees.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Mus",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Mus.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Roodborstje",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Roodborstje.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": "klein",
        "audiopath": "../audio/Roodborstje.mp3",
        "text": "Het roodborstje is een zangvogel die makkelijk te herkennen is aan zijn typerende oranje keel. Opvallend is dat zowel mannetjes als vrouwtjes zingen. Ook zijn beide geslachten heel agressief tegen soortgenoten, waardoor roodborstjes vooral solitair leven."
    },
    {
        "naam": "Kolibri",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Kolibri.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Gronte Bonte Specht",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Grote Bonte Specht.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Winterkoning",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Winterkoning.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Leeuwerik",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Leeuwerik.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Boomkruiper",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Boomkruiper.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Boomklever",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Boomklever.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Tapuit",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Tapuit.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Roodborsttapuit",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Roodborsttapuit.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Gele Kwikstaart",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Gele Kwikstaart.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Kruisbek",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Kruisbek.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Putter",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Putter.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Sijs",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Sijs.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Groenling",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Groenling.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Kwikstaart",
        "categorie": "Kleine Zangvogels",
        "path": "../images/Kwikstaart.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Baardvogel",
        "categorie": "Kleine Zangvogels niet Europa",
        "path": "../images/Baardvogel.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Spitsvogel",
        "categorie": "Kleine Zangvogels niet Europa",
        "path": "../images/Spitsvogel.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Pestvogel",
        "categorie": "Kleine Zangvogels niet Europa",
        "path": "../images/Pestvogel.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Ossenpikker",
        "categorie": "Kleine Zangvogels niet Europa",
        "path": "../images/Ossenpikker.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Pitta",
        "categorie": "Kleine Zangvogels niet Europa",
        "path": "../images/Pitta.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Honingeter",
        "categorie": "Kleine Zangvogels niet Europa",
        "path": "../images/Honingeter.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Liervogel",
        "categorie": "Kleine Zangvogels niet Europa",
        "path": "../images/Liervogel.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Goudhaantje",
        "categorie": "Kleine Zangvogels niet Europa",
        "path": "../images/Goudhaantje.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Spreeuw",
        "categorie": "Grote Zangvogels",
        "path": "../images/Spreeuw.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Rode Kardinaal",
        "categorie": "Grote Zangvogels",
        "path": "../images/Rode Kardinaal.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Raaf",
        "categorie": "Grote Zangvogels",
        "path": "../images/Raaf.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Kraai",
        "categorie": "Grote Zangvogels",
        "path": "../images/Kraai.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Kauw",
        "categorie": "Grote Zangvogels",
        "path": "../images/Kauw.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Ekster",
        "categorie": "Grote Zangvogels",
        "path": "../images/Ekster.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Merel",
        "categorie": "Grote Zangvogels",
        "path": "../images/Merel.jpeg",
        "moeilijkheidsgraad": "makkelijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Lijster",
        "categorie": "Grote Zangvogels",
        "path": "../images/Lijster.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Grote Karekiet",
        "categorie": "Grote Zangvogels",
        "path": "../images/Grote Karekiet.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Roek",
        "categorie": "Grote Zangvogels",
        "path": "../images/Roek.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Bonte Kraai",
        "categorie": "Grote Zangvogels",
        "path": "../images/Bonte Kraai.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Gaai",
        "categorie": "Grote Zangvogels",
        "path": "../images/Gaai.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
    {
        "naam": "Prieelvogel",
        "categorie": "Grote Zangvogels",
        "path": "../images/Prieelvogel.jpeg",
        "moeilijkheidsgraad": "moeilijk",
        "audiocategorie": null,
        "audiopath": null,
        "text": null
    },
]