// Small note: this includes both the image and audio quiz. Some functions need to be reused, others not. This makes
// for some messy code, but for the time being, I just want it to work.

// Import the birds from the data.mjs file.
import { data } from './data.mjs';

// For the audioquiz, we only need birds of which the audiopath is not null.
const dataAudio = data.filter(bird => (bird.audiopath));

// Get elements from HTML.
const question = document.querySelector(".question");
const choices = Array.from(document.querySelectorAll(".choiceText"));

// Get the type of quiz, as defined in quiz_overview.
var selectedDifficulty = localStorage.getItem("difficulty");

// Define all paths to images and audio of the birds (the audio paths need to be filtered)
// for nulls, because not all bird calls have been included.
var imagePaths = data.map(function(bird) {return bird.path});
var audioPaths = data.map(function(bird) {return bird.audiopath}).filter(x => !!x);

// Variables for creating questions.
let indices = [];

// Variables for the quiz.
let currentQuestion = {};
let acceptingAnswers = true;
let questionCounter = 0;
let availableQuestions = [];
let score = 0;
const MAX_QUESTIONS = 5;
let questions = [];

// Helper function to shuffle an array (this is used mostly for getting random indices of the birds).
function shuffleArray(array) {
    let currentIndex = array.length,  randomIndex;
    while (currentIndex != 0) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }
    return array;
}

// Get n random indices (which do not overlap) for the image files.
function getIndicesImages(n) {
    let thisDifficulty = [];
    let otherDifficulty = [];

    for (let i=0; i<data.length;i++) {
        if (data[i].moeilijkheidsgraad == selectedDifficulty) {
            thisDifficulty.push(i);
        } else {
            otherDifficulty.push(i);
        }
    }

    indices = shuffleArray(thisDifficulty).slice(0, n);

    return indices;
}

// Similar than function above, but for audio files (you don't need to check for difficulty here).
function getIndicesAudio(n) {
    let indicesList = [];
    for (let i=0; i<dataAudio.length; i++) {
        indicesList.push(i);
    }

    indices = shuffleArray(indicesList).slice(0, n);
    return indices;
}

// Function to get list of similar birds for images (slightly different for audio).
// First, you get the category and difficulty of the bird, and then gather a list of similar birds
// and similar birds, but at a different difficulty. The reason you do the second, is that when you have
// less than 5 similar birds, you would have an empty multiple choice question. If this is the case, a
// similar bird of the other difficulty is inlcuded.
function getSimilarBirdsImage(data, bird) {
    for (let i=0; i<data.length; i++) {
        if (data[i].naam == bird) {
            var category = data[i].categorie;
            var difficulty = data[i].moeilijkheidsgraad;
        }
    }

    let similarBirds = [];
    let similarBirdsOtherDifficulty = [];
    for (let i=0; i<data.length; i++) {
        if (data[i].categorie == category && data[i].moeilijkheidsgraad == difficulty) {
            similarBirds.push(data[i].naam);
        } else if (data[i].categorie == category && data[i].moeilijkheidsgraad != difficulty) {
            similarBirdsOtherDifficulty.push(data[i].naam);
        }
    }
    return [similarBirds, similarBirdsOtherDifficulty];
}

// Function to get list of similar birds for audio. Works very similarly as for the images, but the categories
// are different.
function getSimilarBirdsAudio(data, bird) {
    // get the audiocategory of the bird
    for (let i=0; i<data.length; i++) {
        if (data[i].naam == bird) {
            var audioCategory = data[i].audiocategorie;
        }
    }

    // get a list of all similar birds for audio
    let similarBirds = [];
    for (let i=0; i<dataAudio.length; i++) {
        if (dataAudio[i].audiocategorie == audioCategory) {
            similarBirds.push(dataAudio[i].naam);
        }
    }
    return similarBirds;
}

// This is a function that returns a list of paths (audio or image) of similar birds, which
// is then used as multiple choice answers.
function getVogelChoices(index) {
    if (selectedDifficulty !== "geluiden") {
        var path = imagePaths[index];
        for (let i=0; i<data.length; i++) {
            if (data[i].path == path) {
                var bird = data[i].naam
            }
        }

        var birdLists = getSimilarBirdsImage(data, bird);
        var similarBirdList = birdLists[0];
        var similarBirdListOtherDifficulty = birdLists[1];
    } else {
        var path = audioPaths[index];
        for (let i=0; i<data.length; i++) {
            if (data[i].audiopath == path) {
                var bird = data[i].naam
            }
        }

        var similarBirdList = getSimilarBirdsAudio(data, bird);
    }

    // If there are enough birds in the difficulty, then just take 5 of those, if not, fill answers from the other difficulties.
    if (similarBirdList.length >= 5) {
        var vogelList = shuffleArray(similarBirdList);
    } else {
        let otherBirds = shuffleArray(similarBirdListOtherDifficulty);
        for (let i=0; i<5-similarBirdList.length; i++) {
            similarBirdList.push(otherBirds[i]);
        }
        var vogelList = shuffleArray(similarBirdList);
    }

    var vogelChoices = vogelList.slice(0, 5);

    // Check that the bird is actually in there, if not, include it.
    if (!vogelChoices.includes(bird)) {
        vogelChoices[4] = bird;
    }

    vogelChoices = shuffleArray(vogelChoices);

    return vogelChoices;
}

// Set up questions: get the paths of the similar birds and turn these into an object, which
// can be used as input for the HTML containers.
function getQuestion(index) {
    for (let i = 0; i < indices.length; i++) {
        if (selectedDifficulty !== "geluiden") {
            var path = imagePaths[index];
            var bird = path.match("images/(.*).jpeg")[1];
        } else {
            var path = audioPaths[index];
            var bird = path.match("audio/(.*).mp3")[1];
        }

        if (selectedDifficulty !== "geluiden") {
            var displayedQuestion = "Welke vogel is dit?";
        } else {
            var displayedQuestion = "Welke vogel maakt dit geluid?";
        }

        var vogelChoices = getVogelChoices(index)

        var element = {
            imageAudio: path,
            question: displayedQuestion,
            choice1: vogelChoices[0],
            choice2: vogelChoices[1],
            choice3: vogelChoices[2],
            choice4: vogelChoices[3],
            choice5: vogelChoices[4],
            answer: bird
        };

        return element;
    }
}

// This function starts the game, by denoting which divs should be visible (based on images vs geluiden) and
// it calls the functions that create questions. It also keeps track of the questions and score.
function startGame () {
    if (selectedDifficulty !== "geluiden") {
        indices = getIndicesImages(5);
        document.querySelector(".questionImage").style.display = "inline";
        document.querySelector(".questionAudio").style.display = "none";
    } else {
        indices = getIndicesAudio(5);
        document.querySelector(".questionImage").style.display = "none";
        document.querySelector(".questionAudio").style.display = "flex";
    }

    for (let i=0; i<indices.length; i++) {
        var element = getQuestion(indices[i]);
        questions.push(element);
    }
    
    questionCounter = 0;
    score = 0;
    availableQuestions = [...questions];
    getNewQuestion();
}

// This function checks if the game has ended and if it has, turns of some divs and makes others apear.
function checkForEnd() {
    if(availableQuestions.length === 0 || questionCounter > MAX_QUESTIONS) {
        document.querySelector(".duringQuestions").style.display = "none";
        document.querySelector(".questionAlt").style.display = "none";
        document.querySelector(".end").style.display = "inline";

        const endImageIndex = Math.floor(Math.floor(Math.random() * 5) + 1);
        let endImageSrc = "../images/end"+endImageIndex+".jpeg";
        document.querySelector(".image").src=endImageSrc;
        document.querySelector(".titleScore").innerText = `Jouw Score: ${score}/5`;
        
        // end audio if relevant and for the geluidenquiz, the display image is a bit different
        if (selectedDifficulty === "geluiden") {
            let audio = document.querySelector(".audio");
            audio.pause();
            document.querySelector(".questionImage").style.display = "inline";
            document.querySelector(".questionAudio").style.display = "none";
        }
    }
}

// This function changes the sources of images or audio for a question. For the geluidenquiz, I also make 
// the logo clickable and make sure that the logo and mediaplayer run simultaniously.
function changeImageOrAudioSource() {
    if (selectedDifficulty !== "geluiden") {
        let imageSrc = currentQuestion.imageAudio;
        document.querySelector(".image").src=imageSrc;
    } else {
        let audioSrc = currentQuestion.imageAudio;
        document.querySelector(".audio").src=audioSrc;

        let audio = document.getElementById("audioBird");
        audio.src = audioSrc;
        let mediaPlayer = document.querySelector(".audio");
        document.querySelector(".audioLogo")
                .addEventListener("click", function play(e) {
                    // weird problem where the number of clicks is counted as many times
                    // as the question (i.e this loop happens 4 times at question 4). This
                    // stopImmediatePropagation solves this
                    e.stopImmediatePropagation();
                    if (audio.paused) {
                        audio.play();
                        mediaPlayer.play();
                    } else {
                        audio.pause();
                        mediaPlayer.pause();
                    }
        })
    }
}

// This function first checks if the game is over, by calling the checkForEnd function and if
// the game is not done, it changes the text of the question, the answers and the source of the
// image or audio file.
function getNewQuestion () {
    checkForEnd();

    questionCounter++;

    const questionIndex = Math.floor(Math.random() * availableQuestions.length);
    currentQuestion = availableQuestions[questionIndex];
    question.innerText = currentQuestion.question;
    changeImageOrAudioSource();

    choices.forEach(choice => {
        const number = choice.dataset["number"];
        choice.innerText = currentQuestion["choice" + number];
    })

    availableQuestions.splice(questionIndex, 1);

    acceptingAnswers = true;
}

// This code checks for the correct answers. If the correct answer is clicked, it turns green and if
// not, it turns red and the correct answer lights up.
choices.forEach(choice => {
    choice.addEventListener("click", e => {
        if(!acceptingAnswers) return;

        acceptingAnswers = false;
        const selectedChoice = e.target;
        const selectedAnswer = choice.innerText;

        let classToApply = selectedAnswer == currentQuestion.answer ? 'correct' : 'incorrect';

        if(classToApply === 'correct') {
            score++;
            selectedChoice.parentElement.style.background = 'MediumSpringGreen';

            setTimeout(() => {
                choices.forEach(choice => {
                    choice.parentElement.style.background = 'WhiteSmoke';
                })
                getNewQuestion();
            }, 750);
        } else {
            selectedChoice.parentElement.style.background = 'OrangeRed';
            choices.forEach(choice => {
                if (choice.textContent == currentQuestion.answer) {
                    choice.parentElement.style.background = 'MediumSpringGreen';
                }
            })

            setTimeout(() => {
                choices.forEach(choice => {
                    choice.parentElement.style.background = 'WhiteSmoke';
                })
                getNewQuestion();
            }, 1200);
        }
    })
})

startGame();